make

rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

for good in examples/good/*.cpc; do
  echo "Running $good. This should succeed"
  cat "$good" | ./interpreter
done

for bad in examples/bad/*.cpc; do
  echo "Running $bad. This should fail"
  cat "$bad" | ./interpreter
done