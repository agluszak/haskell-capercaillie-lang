{-# LANGUAGE BlockArguments #-}
module Main where

import System.Environment ( getArgs, getProgName )
import System.Exit ( exitFailure, exitSuccess )
import Control.Monad (when)

import Lexer
import Parser
import ParsedAST
import ErrM
import ASTTransformer (transformProgram, ParsedPos)
import Typechecker
import Interpreter
import Control.Monad.Cont (liftIO)
import AST
import StdLib
import System.IO

type ParseFun a = [Token] -> Err a

type Verbosity = Int

runFile :: Verbosity -> ParseFun (Program ParsedPos) -> FilePath -> IO ()
runFile v p f = readFile f >>= run v p

run :: Verbosity -> ParseFun (Program ParsedPos) -> String -> IO ()
run v p s = let ts = myLexer s in case p ts of
  Left err    -> do 
    hPutStrLn stderr err
    exitFailure
  Right  tree -> do 
    let ast = transformProgram tree
    case typecheckProgram (emptyEnv{vals=stdTypes}) ast of 
      Left err -> do
        hPrint stderr err
        exitFailure
      Right env -> do
        result <- runProgram (emptyRuntimeEnv{values=stdVals}) ast
        either (hPrint stderr) print result
        exitSuccess



usage :: IO ()
usage = do
  putStrLn $ unlines
    [ "usage:"
    , "  --help          Display this help message."
    , "  (no arguments)  Parse stdin."
    , "  (file)          Parse content of file."
    ]
  exitFailure

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["--help"] -> usage
    [] -> getContents >>= run 2 pProgram
    fs -> mapM_ (runFile 2 pProgram) fs
