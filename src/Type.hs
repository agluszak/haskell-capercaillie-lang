{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE NamedFieldPuns #-}
module Type where
import Common

data Kind = Star | KindFunction Kind Kind
  deriving (Eq, Ord)

makeKind :: [a] -> Kind
makeKind = foldr (\_ s -> KindF s) Star

instance Show Kind where
  showsPrec _ Star = showString "*"
  showsPrec d (KindFunction k1 k2) = showParen (d > 10) $
                                                    showsPrec 11 k1 . showString " -> " . showsPrec 10 k2

data TCon = TCon Name Kind
  deriving (Eq, Ord)

data TVar = TVar Name Kind
  deriving (Eq, Ord)

data Type = TypeVar TVar
  | TypeConstructor TCon
  | TypeApplication Type Type
  | TypeReplaceable Int
  deriving (Eq, Ord)

data Scheme = Forall [Kind] Type
  deriving (Eq, Show)

toScheme :: Type -> Scheme
toScheme = Forall []

pattern KindF :: Kind -> Kind
pattern KindF ret = KindFunction Star ret
pattern KindF2 :: Kind
pattern KindF2 = KindF (KindF Star)

pattern TypeData :: String -> Type
pattern TypeData name = TypeConstructor (TCon name Star)

pattern TypeUnit :: Type
pattern TypeUnit = TypeData "Unit"
pattern TypeBoolean :: Type
pattern TypeBoolean = TypeData "Boolean"
pattern TypeInt :: Type
pattern TypeInt = TypeData "Int"
pattern TypeString :: Type
pattern TypeString = TypeData "String"
pattern TypeFunction :: Type -> Type -> Type
pattern TypeFunction a b = TypeApplication (TypeApplication (TypeConstructor (TCon "->" KindF2)) a) b

instance Show TCon where
  show (TCon con k) = con ++ "(" ++ show k ++ ")"

instance Show TVar where
  show (TVar var k) = "'" ++ var ++ "(" ++ show k ++ ")"

instance Show Type where
  showsPrec d (TypeConstructor con) = shows con
  showsPrec _ (TypeVar var) = shows var
  showsPrec _ (TypeReplaceable i) = shows i
  showsPrec d (TypeFunction arg result) = showParen (d > 10) $
            showsPrec 11 arg . showString " -> " . showsPrec 10 result
  showsPrec d (TypeApplication t1 t2) = showParen (d > 10) $ showsPrec 11 t1 . showString " ". showsPrec 10 t2
  
class HasKind t where
  kind :: t -> Kind

instance HasKind TVar where
  kind (TVar _ k) = k

instance HasKind TCon where
  kind (TCon _ k) = k

instance HasKind Type where
  kind (TypeConstructor tc) = kind tc
  kind (TypeVar u)  = kind u
  kind (TypeApplication t _) = case kind t of
    (KindFunction _ k) -> k
