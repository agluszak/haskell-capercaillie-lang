module TCError where

import AST
import Type
import Common

data TCErrorKind = CannotUnifyError Type Type
  | KindMismatchError TVar Type
  | CannotSubtituteError TVar Type
  | UnknownTypeError Name
  | UnknownValError Name
  | DuplicateDataTypeError Name
  | DuplicateConstructorError Name
  | ConstructorDefinedTwiceError

instance Show TCErrorKind where
  show (CannotUnifyError t1 t2) = "Cannot unify types " ++ show t1 ++ " and " ++ show t2
  show (KindMismatchError var t) = "Kind mismatch. Type " ++ show var ++ " has a different kind than type " ++ show t
  show (CannotSubtituteError var t) = "Cannot subtitute type " ++ show var ++ " for type " ++ show t
  show (UnknownTypeError name) = "Unknown type " ++ show name
  show (UnknownValError name) = "Unknown val " ++ show name
  show (DuplicateDataTypeError name) = "Type " ++name ++ " already defined"
  show (DuplicateConstructorError name) = "Constructor " ++ name ++ " already defined"
  show ConstructorDefinedTwiceError = "Conflicting definitions of a constructor"

data TCError = TCError TCErrorKind Pos

instance Show TCError where
  show (TCError kind (line, column)) = "Error. " ++ show kind ++ " at " ++ show line ++ ":" ++ show column