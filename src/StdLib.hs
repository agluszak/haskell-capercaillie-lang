{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BlockArguments #-}
module StdLib where
import AST
import TCError
import Interpreter
import qualified Data.Map  as Map
import Control.Monad.Error.Class
import Control.Monad.Cont (liftIO)
import Common
import Type

data StdDef = StdDef Name Type Val

defineNativeFun :: Name -> [Type] -> Type -> InterpreterM Val -> StdDef
defineNativeFun name paramTypes returnType native = StdDef name tpe val where
  tpe = foldType paramTypes returnType
  foldType [] ret = ret
  foldType (h:t) ret = TypeFunction h (foldType t ret)
  val = lambdify (zipWith (\a b -> "$" ++ show b) paramTypes [1..]) native emptyRuntimeEnv

defineNativeVal :: Name -> Type -> Val -> StdDef
defineNativeVal name tpe val = StdDef name tpe val

addDef = defineNativeFun "add" [TypeInt, TypeInt] TypeInt (do
                                                          (VInt _1) <- lookupRuntimeVal "$1"
                                                          (VInt _2) <- lookupRuntimeVal "$2"
                                                          return $ VInt (_1 + _2))
subDef = defineNativeFun "sub" [TypeInt, TypeInt] TypeInt (do
                                                          (VInt _1) <- lookupRuntimeVal "$1"
                                                          (VInt _2) <- lookupRuntimeVal "$2"
                                                          return $ VInt (_1 - _2))
mulDef = defineNativeFun "mul" [TypeInt, TypeInt] TypeInt (do
                                                         (VInt _1) <- lookupRuntimeVal "$1"
                                                         (VInt _2) <- lookupRuntimeVal "$2"
                                                         return $ VInt (_1 * _2))
divDef = defineNativeFun "div" [TypeInt, TypeInt] TypeInt (do
                                                         (VInt _1) <- lookupRuntimeVal "$1"
                                                         (VInt _2) <- lookupRuntimeVal "$2"
                                                         if _2 == 0 then ierror DivisionByZero else return $ VInt (_1 `div` _2))
concatDef = defineNativeFun "concat" [TypeString, TypeString] TypeString (do
                                                                       (VString _1) <- lookupRuntimeVal "$1"
                                                                       (VString _2) <- lookupRuntimeVal "$2"
                                                                       return $ VString (_1 ++ _2))
lengthDef = defineNativeFun "length" [TypeString] TypeInt (do
                                                            (VString _1) <- lookupRuntimeVal "$1"
                                                            return $ VInt (toInteger $ length _1))
btsDef = defineNativeFun "bts" [TypeBoolean] TypeString (do
                                                        _1 <- lookupRuntimeVal "$1"
                                                        return case _1 of
                                                          VTrue ->  VString "True"
                                                          VFalse -> VString "False")
itsDef = defineNativeFun "its" [TypeInt] TypeString (do
                                                   (VInt _1) <- lookupRuntimeVal "$1"
                                                   return $ VString (show _1))
notDef = defineNativeFun "not" [TypeBoolean] TypeBoolean (do
                                                        _1 <- lookupRuntimeVal "$1"
                                                        return case _1 of
                                                          VTrue -> VFalse
                                                          VFalse -> VTrue)
andDef = defineNativeFun "and" [TypeBoolean, TypeBoolean] TypeBoolean (do
                                                                     _1 <- lookupRuntimeVal "$1"
                                                                     case _1 of
                                                                       VFalse -> return VFalse
                                                                       VTrue -> lookupRuntimeVal "$2")

orDef = defineNativeFun "or" [TypeBoolean, TypeBoolean] TypeBoolean (do
                                                                     _1 <- lookupRuntimeVal "$1"
                                                                     case _1 of
                                                                       VTrue -> return VTrue
                                                                       VFalse -> lookupRuntimeVal "$2")

eqbDef = defineNativeFun "eqb" [TypeBoolean, TypeBoolean] TypeBoolean (do
                                                                     _1 <- lookupRuntimeVal "$1"
                                                                     _2 <- lookupRuntimeVal "$2"
                                                                     case (_1, _2) of
                                                                        (VTrue, VFalse) -> return VTrue
                                                                        (VFalse, VFalse) -> return VTrue
                                                                        _ -> return VFalse)

gtDef = defineNativeFun "gt" [TypeInt, TypeInt] TypeBoolean (do
                                                               (VInt _1) <- lookupRuntimeVal "$1"
                                                               (VInt _2) <- lookupRuntimeVal "$2"
                                                               return $ if _1 > _2 then VTrue else VFalse)

eqiDef = defineNativeFun "eqi" [TypeInt, TypeInt] TypeBoolean (do
                                                               (VInt _1) <- lookupRuntimeVal "$1"
                                                               (VInt _2) <- lookupRuntimeVal "$2"
                                                               return $ if _1 == _2 then VTrue else VFalse)

printDef = defineNativeFun "print" [TypeString] TypeUnit (do
                                                        (VString _1) <- lookupRuntimeVal "$1"
                                                        liftIO $ putStr _1
                                                        return VUnit)
trueDef = defineNativeVal "True" TypeBoolean VTrue

falseDef = defineNativeVal "False" TypeBoolean VFalse

stdDefs = [addDef, subDef, mulDef, divDef, concatDef, lengthDef, btsDef, itsDef, notDef, 
  andDef, orDef, eqbDef, gtDef, eqiDef, printDef, trueDef, falseDef]

stdTypes = Map.fromList $ map (\(StdDef name tpe _) -> (name, toScheme tpe)) stdDefs

stdVals = Map.fromList $ map (\(StdDef name _ val) -> (name, val)) stdDefs
