{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE NamedFieldPuns #-}
module Typechecker where

import AST
import Control.Monad.Except
import TCError
import Control.Monad.Reader
import Control.Monad.State.Lazy
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.List as List
import Common
import Data.Bifunctor (second)
import Type
import Debug.Trace
import Data.List (intersect)

type Counter = Int

type Subst = [(TVar, Type)]

data TCState = TCState {
  subst :: Subst,
  counter :: Counter
}

emptyState :: TCState
emptyState = TCState { subst = emptySubst, counter = 0 }

type TypecheckerM a = ExceptT TCError (ReaderT Env (State TCState)) a

data Env = Env {
  dataTypes :: Set.Set Name,
  vals :: Map.Map Name Scheme,
  pos :: Pos
} deriving (Show)

movePos :: Pos -> Env -> Env
movePos newPos env@Env{} = env{pos=newPos}

emptyEnv :: Env
emptyEnv = Env {
  dataTypes = Set.empty,
  vals = Map.empty,
  pos = (0,0)
}

tcError :: TCErrorKind -> TypecheckerM a
tcError err = do
  pos <- asks pos
  throwError $ TCError err pos

enumTypeVar :: Int -> Name
enumTypeVar n = "$T" ++ show n

freshTypeVar :: Kind -> TypecheckerM Type
freshTypeVar k = state (\s@TCState{counter} -> let tVar = TVar (enumTypeVar counter) k 
                                                in (TypeVar tVar, s{counter=counter+1}))

lookupVal :: String -> TypecheckerM Scheme
lookupVal name = do
  maybeVal <- asks (Map.lookup name . vals)
  maybe (tcError $ UnknownValError name) return maybeVal

addVal :: Name -> Scheme -> Env -> Env
addVal name tpe env@Env{vals} = env{vals=vals'}
  where vals' = Map.insert name tpe vals

typecheckExpr :: Expr -> TypecheckerM Type
typecheckExpr EIntLit{} = return TypeInt
typecheckExpr EStringLit{} = return TypeString
typecheckExpr EVal{targetName, exprPos} = local (movePos exprPos) $ do
  scheme <- lookupVal targetName
  instantiate scheme
typecheckExpr ELambda {parameter = Parameter{parameterName, parameterType}, body, exprPos} = local (movePos exprPos) $ do
  t <- freshTypeVar Star
  let sc = toScheme t
  tBody <- local (addVal parameterName sc) $ typecheckExpr body
  return $ TypeFunction t tBody
typecheckExpr EApp {target, argument, exprPos} = local (movePos exprPos) $ do
  tTarget <- typecheckExpr target
  tArg <- typecheckExpr argument
  t <- freshTypeVar Star
  unify (TypeFunction tArg t) tTarget
  return t
typecheckExpr EIf {predicate, ifTrue , ifFalse, exprPos} = local (movePos exprPos) $ do
  tPredicate <- typecheckExpr predicate
  unify tPredicate TypeBoolean
  tIfTrue <- typecheckExpr ifTrue
  tIfFalse <- typecheckExpr ifFalse
  unify tIfTrue tIfFalse
  return tIfTrue
typecheckExpr EDef {def, body, exprPos} = local (movePos exprPos) $ do
  env <- typecheckDef def
  local (const env) $ typecheckExpr body
typecheckExpr EMatch {target, cases, exprPos} = local (movePos exprPos) $ do
  tTarget <- typecheckExpr target
  tRet <- freshTypeVar Star
  tCases <- mapM (typecheckCase tTarget) cases
  mapM_ (unify tRet) tCases
  return tRet


typecheckCase :: Type -> Case -> TypecheckerM Type
typecheckCase targetType (Case pat expr pos) = local (movePos pos) $ do
  transform <- typecheckPattern targetType pat
  local transform $ typecheckExpr expr

typecheckPattern :: Type -> Pattern -> TypecheckerM (Env -> Env)
typecheckPattern tpe (PCapture name pos) = local (movePos pos) $ return $ addVal name (toScheme tpe)
typecheckPattern tpe (PStringLit _ pos) = local (movePos pos) $ unify tpe TypeString >> return id
typecheckPattern tpe (PIntLit _ pos) = local (movePos pos) $ unify tpe TypeInt >> return id
typecheckPattern tpe (PData constructorName subpatterns pos) = local (movePos pos) $ do
  constructorScheme <- lookupVal constructorName
  constructorType <- instantiate constructorScheme
  constructorParameters <- mapM (\_ -> freshTypeVar Star) subpatterns
  envTransforms <- zipWithM typecheckPattern constructorParameters subpatterns
  sub <- getSubst
  let constructorParameters' = applySubst sub constructorParameters
  unify constructorType (foldr TypeFunction tpe constructorParameters')
  return $ foldr (.) id envTransforms


typecheckDef :: Def -> TypecheckerM Env
typecheckDef vd@ValDef {valName, valBody, valType, defPos} = local (movePos defPos) $ do
  tThis <- freshTypeVar Star
  tBody <- local (addVal valName $ toScheme tThis) $ typecheckExpr valBody -- handle recursive calls
  unify tThis tBody
  sub <- getSubst
  env <- ask
  let tBody' = applySubst sub tBody
  let tVars = typeVars tBody' List.\\ typeVars (applySubst sub env) 
  let sc = makeReplaceable tVars tBody'
  return $ addVal valName sc env
typecheckDef dd@DataDef{dataName, dataConstructors, dataTypeParams, defPos} = local (movePos defPos) $ do
  env@Env{dataTypes, vals} <- ask
  ensureUniqueType dataName
  ensureNoDuplicateConstructors (map constructorName dataConstructors)
  mapM_ (\DataConstructor{constructorName} -> ensureUniqueConstructor constructorName) dataConstructors
  let tData = makeDataType dataName dataTypeParams
  let dataTypes' = Set.insert dataName dataTypes
  let vals' = Map.union vals (makeConstructors tData dataTypeParams dataConstructors)
  return $ env{dataTypes=dataTypes', vals=vals'}
    
    
makeConstructors :: Type -> [Name] -> [DataConstructor] -> Map.Map Name Scheme
makeConstructors tData dataTypeParams dataConstructors = 
  Map.fromList $ map (\c -> (constructorName c, constructorFunType dataTypeParams tData c)) dataConstructors

makeDataType :: Name -> [Name] -> Type
makeDataType name params = 
    foldr (flip TypeApplication . (\ n -> TypeVar $ TVar n Star)) (TypeConstructor $ TCon name (makeKind params)) params
    
  
ensureNoDuplicateConstructors :: [Name] -> TypecheckerM ()
ensureNoDuplicateConstructors names = 
  when (length names /= length (Set.fromList names)) $ tcError ConstructorDefinedTwiceError

ensureUniqueType :: Name -> TypecheckerM ()
ensureUniqueType dataName = do 
  env@Env{dataTypes} <- ask
  when (Set.member dataName dataTypes) $ tcError (DuplicateDataTypeError dataName)
                         
ensureUniqueConstructor :: Name -> TypecheckerM ()
ensureUniqueConstructor constructorName = do
  env@Env{vals} <- ask
  when (Set.member constructorName (Map.keysSet vals)) $ tcError (DuplicateConstructorError constructorName)
  
constructorFunType :: [Name] -> Type -> DataConstructor -> Scheme
constructorFunType typeParams retType DataConstructor{constructorParameters} = 
  makeTypeParamsReplaceable typeParams t
  where 
    t = foldr (TypeFunction . typedParameterType) retType constructorParameters


emptySubst :: Subst
emptySubst = []

substAs :: TVar -> Type -> Subst
substAs var t = [(var, t)]

composeSubst :: Subst -> Subst -> Subst
composeSubst s1 s2 = map (Data.Bifunctor.second (applySubst s1)) s2 ++ s1

getSubst :: TypecheckerM Subst
getSubst = gets subst

addSubst :: Subst -> TypecheckerM ()
addSubst sub = modify (\s@TCState{subst} -> let subst' = composeSubst sub subst in s{subst=subst'})

mgu :: Type -> Type -> TypecheckerM Subst
mgu (TypeApplication arg1 ret1) (TypeApplication arg2 ret2) = do
  s1 <- mgu arg1 arg2
  s2 <- mgu (applySubst s1 ret1) (applySubst s1 ret2)
  return $ composeSubst s2 s1
mgu (TypeVar var) t = substVar var t
mgu t (TypeVar var) = substVar var t
mgu (TypeConstructor tc1) (TypeConstructor tc2) | tc1 == tc2 = return emptySubst
mgu t1 t2 = tcError (CannotUnifyError t1 t2)

substVar :: TVar -> Type -> TypecheckerM Subst
substVar var t | t == TypeVar var = return emptySubst
               | kind var /= kind t = tcError $ KindMismatchError var t
               | var `elem` typeVars t = tcError $ CannotSubtituteError var t
               | otherwise = return $  var `substAs` t

unify :: Type -> Type -> TypecheckerM ()
unify t1 t2 = do
  s <- getSubst
  u <- mgu (applySubst s t1) (applySubst s t2)
  addSubst u

class Types t where
  applySubst :: Subst -> t -> t
  typeVars :: t -> [TVar]

instance Types Type where
  applySubst s (TypeVar var) = case lookup var s of
    Just t -> t
    Nothing -> TypeVar var
  applySubst s (TypeApplication arg res) = TypeApplication (applySubst s arg) (applySubst s res)
  applySubst _ t = t

  typeVars (TypeVar var) = [var]
  typeVars (TypeApplication arg ret) = typeVars arg `List.union` typeVars ret
  typeVars _ = []

instance Types a => Types [a] where
  applySubst s = map (applySubst s)
  typeVars = List.nub . concatMap typeVars

instance Types Env where
  applySubst s env@Env{vals} = let vals' = fmap (applySubst s) vals in env{vals=vals'}
  typeVars env@Env{vals} = typeVars (Map.elems vals)

instance Types Scheme where
  applySubst s (Forall kinds t) = Forall kinds (applySubst s t)
  typeVars (Forall _ t) = typeVars t

makeReplaceable :: [TVar] -> Type -> Scheme
makeReplaceable vars t = Forall kinds (applySubst s t)
  where replaceable = typeVars t `intersect` vars
        kinds = map kind replaceable
        s = zip replaceable (map TypeReplaceable [0..])

makeTypeParamsReplaceable :: [Name] -> Type -> Scheme
makeTypeParamsReplaceable typeParams
  = makeReplaceable (map (`TVar` Star) typeParams)

instantiate :: Scheme -> TypecheckerM Type
instantiate (Forall kinds t) = do
  ts <- mapM freshTypeVar kinds
  return (inst ts t)
  where
    inst :: [Type] -> Type -> Type
    inst ts (TypeApplication arg ret) = TypeApplication (inst ts arg) (inst ts ret)
    inst ts (TypeReplaceable n ) = ts !! n
    inst _ t = t

typecheckProgram :: Env -> Expr -> Either TCError Env
typecheckProgram env expr = evalState (runReaderT (runExceptT prog) env) emptyState where
  prog = do
    typecheckExpr expr
    s <- getSubst
    asks (applySubst s)
