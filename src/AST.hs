{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternSynonyms #-}
module AST where
import  qualified Data.Map as Map
import           Control.Monad.Reader
import Control.Monad.Error.Class
import Debug.Trace (traceShow)
import Data.List (intercalate)
import Common
import Type
import Control.Monad.Except (ExceptT)

data Def = DataDef {
             dataName :: Name,
             dataConstructors :: [DataConstructor], 
             dataTypeParams :: [Name],
             defPos :: Pos
           } | ValDef {
             valName :: Name,
             valBody :: Expr,
             valType :: Maybe Type,
             valTypeParams :: [Name],
             defPos :: Pos
           } deriving (Show)

data DataConstructor = DataConstructor {
  constructorName :: Name,
  constructorParameters :: [TypedParameter],
  constructorPos :: Pos
} deriving (Show)

data TypedParameter = TypedParameter {
  typedParameterName :: Name,
  typedParameterType :: Type,
  typedParameterPos :: Pos
} deriving (Show)

data Parameter = Parameter {
  parameterName :: Name,
  parameterType :: Maybe Type,
  parameterPos :: Pos
} deriving (Show)

data Case = Case Pattern Expr Pos deriving (Show)

data Pattern = PIntLit { pival :: Integer, patPos :: Pos } 
  | PStringLit { psval :: String, patPos :: Pos }  
  | PData {patName :: Name, subpatterns :: [Pattern], patPos :: Pos }
  | PCapture {patName :: Name, patPos :: Pos} 
  deriving (Show)

data Expr = EIntLit { ival :: Integer, exprPos :: Pos}
  | EStringLit { sval :: String, exprPos :: Pos}
  | ELambda { parameter :: Parameter, body ::Expr, exprPos :: Pos}
  | EVal { targetName :: Name, exprPos :: Pos}
  | EApp {target :: Expr, argument :: Expr, exprPos :: Pos}
  | EIf {predicate :: Expr, ifTrue :: Expr, ifFalse :: Expr, exprPos :: Pos}
  | EDef { def :: Def, body :: Expr, exprPos :: Pos}
  | EMatch {target :: Expr, cases :: [Case], exprPos :: Pos}
  deriving (Show)
