{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE PatternSynonyms #-}
module Interpreter where
import TCError
import AST
import           Control.Monad.Trans.Maybe
import  qualified Data.Map as Map
import           Control.Monad.Reader
import Control.Monad.Error.Class
import Data.Maybe (isJust, fromMaybe, fromJust)
import Control.Monad.Trans.Except (runExceptT)
import Debug.Trace (traceShow, trace)
import Common
import Type
import Control.Monad.Except (ExceptT)
import Data.List (intercalate)


data Val =
  VInt Integer
  | VString  String
  | VLambda (Val -> InterpreterM Val)
  | VData Name [Val]

pattern VTrue :: Val
pattern VTrue = VData "True" []
pattern VFalse :: Val
pattern VFalse = VData "False" []
pattern VUnit :: Val
pattern VUnit = VData "Unit" []
  
instance Show Val where
  show (VInt val) = show val
  show (VString val) = show val
  show (VLambda _) = "<fn>"
  show (VData con args) = con ++ "(" ++ intercalate "," (map show args) ++ ")"

data IErrorKind = DivisionByZero | PatternMatchingFailed Val

instance Show IErrorKind where
  show DivisionByZero = "Division by 0"
  show (PatternMatchingFailed val) = "Value " ++ show val ++ " could not be matched"

data IError = IError IErrorKind Pos

instance Show IError where
  show (IError kind (line, column)) = "Runtime error. " ++ show kind ++ " at " ++ show line ++ ":" ++ show column

ierror :: IErrorKind -> InterpreterM a
ierror kind = do
  pos <- asks pos
  throwError $ IError kind pos

data RuntimeEnv = RuntimeEnv {
  values :: Map.Map Name Val,
  pos :: Pos
} deriving (Show)

movePos :: Pos -> RuntimeEnv -> RuntimeEnv
movePos newPos env@RuntimeEnv{} = env{pos=newPos}

type InterpreterM a = ExceptT IError (ReaderT RuntimeEnv IO) a

lookupRuntimeVal :: Name -> InterpreterM Val
lookupRuntimeVal name = asks (fromMaybe (error $ "FATAL: Val not found " ++ name) . Map.lookup name . values)

addRuntimeVal :: Name -> Val -> RuntimeEnv -> RuntimeEnv
addRuntimeVal name val env@RuntimeEnv{values} = env{values=Map.insert name val values}

interpretDef :: Def -> InterpreterM (RuntimeEnv -> RuntimeEnv)
interpretDef  ValDef {valName, valBody, defPos} = local (movePos defPos) $ mdo
  envTransform <- return $ addRuntimeVal valName valBody'
  valBody' <- local envTransform $ interpretExpr valBody
  return $ addRuntimeVal valName valBody'
interpretDef DataDef{dataName, dataConstructors, defPos} = local (movePos defPos) $ do
  let constructorFuns = map (\con -> (constructorName con, constructorFun con)) dataConstructors
  let envFuns = map (uncurry addRuntimeVal) constructorFuns
  return $ foldr (.) id envFuns
  where 
    constructorFun DataConstructor{constructorParameters=[], constructorName} = VData constructorName []
    constructorFun DataConstructor{constructorParameters, constructorName} = 
      lambdify paramNames construct emptyRuntimeEnv
      where
      paramNames = map typedParameterName constructorParameters
      construct = do
        args <- mapM lookupRuntimeVal paramNames
        return $ VData constructorName args

lambdify :: [String] -> InterpreterM Val -> RuntimeEnv -> Val
lambdify (h1:h2:t) thunk env = VLambda (\arg -> return $ lambdify (h2:t) thunk (addRuntimeVal h1 arg env))
lambdify [h] thunk env = VLambda (\arg -> local (const (addRuntimeVal h arg env)) thunk)

tryMatch :: Val -> [Case] -> InterpreterM Val
tryMatch val [] = ierror $ PatternMatchingFailed val
tryMatch val (h:t) = fromMaybe (tryMatch val t) (match val h )

submatch :: Val -> Pattern -> Maybe (RuntimeEnv -> RuntimeEnv)
submatch v (PCapture name _) = Just $ addRuntimeVal name v
submatch (VInt i) (PIntLit pi _) = if pi == i then Just id else Nothing
submatch (VString s) (PStringLit ps _) = if ps == s then Just id else Nothing
submatch (VData con args) (PData pcon pargs _) =
  if con == pcon then foldr (liftM2 (.) . uncurry submatch) (Just id) (zip args pargs) else Nothing

match :: Val -> Case -> Maybe (InterpreterM Val)
match val (Case pat expr pos) =  fmap (run expr) (submatch val pat)
  where run expr envFun = local envFun (interpretExpr expr)
  
interpretExpr :: Expr -> InterpreterM Val
interpretExpr EMatch {cases, target, exprPos} = local (movePos exprPos) $ do
  target' <- interpretExpr target
  tryMatch target' cases
interpretExpr EDef {def, body, exprPos} = local (movePos exprPos) $ do
  env <- ask
  envTransform <- interpretDef def
  local envTransform $ interpretExpr body
interpretExpr EIntLit {ival} = return $ VInt ival
interpretExpr EStringLit {sval}= return $ VString sval
interpretExpr ELambda{parameter, body, exprPos} = local (movePos exprPos) $ do 
  env <- ask
  return $ lambdify [parameterName parameter] (interpretExpr body) env
interpretExpr EVal {targetName} = lookupRuntimeVal targetName
interpretExpr EApp {target, argument, exprPos} = local (movePos exprPos) $ do
  target' <- interpretExpr target
  argument' <- interpretExpr argument
  case target' of 
    VLambda f -> f argument'
interpretExpr EIf {predicate, ifTrue, ifFalse, exprPos} = local (movePos exprPos) $ do
  predicate' <- interpretExpr predicate
  case predicate' of VTrue -> interpretExpr ifTrue
                     VFalse -> interpretExpr ifFalse

emptyRuntimeEnv :: RuntimeEnv
emptyRuntimeEnv = (RuntimeEnv {values=Map.empty, pos=(0,0)})

runProgram :: RuntimeEnv -> Expr -> IO (Either IError Val)
runProgram env p = runReaderT (runExceptT (interpretExpr p)) env