module ASTTransformer where

import qualified ParsedAST as P
import AST
import Common
import Type

type ParsedPos = Maybe (Int, Int)

transformMaybeTyped :: P.MaybeTyped ParsedPos -> Maybe Type
transformMaybeTyped (P.Typed _ tpe) = Just $ transformType Star tpe
transformMaybeTyped (P.NotTyped _) = Nothing


-- FIXME return type declaration in function def is ignored
transformDef :: P.Def ParsedPos -> Def
transformDef (P.DVal (Just pos) (P.LIdent name) tpe expr) = ValDef { valName = name, valType = transformMaybeTyped tpe, valBody = transformExpr expr, valTypeParams = [], defPos = pos}
transformDef (P.DGenericVal (Just pos) (P.LIdent name) typeParams tpe expr) = ValDef { valName = name, valType = transformMaybeTyped tpe, valBody = transformExpr expr, valTypeParams = map (\(P.UIdent name) -> name ) typeParams, defPos = pos}
transformDef (P.DFun (Just pos) (P.LIdent name) params tpe expr) = ValDef { valName = name, valType = Nothing,
  valBody = transformLambda pos params expr, valTypeParams = [], defPos = pos}
transformDef (P.DGenericFun (Just pos) (P.LIdent name) typeParams params tpe expr) = ValDef { valName = name, valType = Nothing,
  valBody = transformLambda pos params expr,  valTypeParams = map (\(P.UIdent name) -> name ) typeParams, defPos = pos}
transformDef (P.DType (Just pos) (P.UIdent name) tpe) = error "Type synonyms are not supported at this time"
transformDef (P.DGenericType (Just pos) (P.UIdent name) typeParams tpe) = error "Type synonyms are not supported at this time"
transformDef (P.DData (Just pos) (P.UIdent name) constructors) = DataDef { dataName = name, dataConstructors = map transformConstructor constructors, defPos = pos, dataTypeParams = []}
transformDef (P.DGenericData (Just pos) (P.UIdent name) typeParams constructors) = DataDef { dataName = name, dataConstructors = map transformConstructor constructors, defPos = pos, dataTypeParams = map (\(P.UIdent name) -> name ) typeParams}

transformType :: Kind -> P.Type ParsedPos  -> Type
transformType k (P.TData (Just pos) (P.UIdent name)) = TypeConstructor $ TCon name k
transformType k (P.TArgument (Just pos) (P.UIdent name)) = TypeVar $ TVar name k
transformType k (P.TGenericData (Just pos) tpe typeParams) =
  foldr (flip TypeApplication) (transformType (makeKind typeParams) tpe) (map (transformType Star) typeParams)
transformType k (P.TFunction (Just pos) arg ret) = TypeFunction (transformType Star arg) (transformType Star ret)

transformConstructor :: P.CaseDef ParsedPos -> DataConstructor
transformConstructor (P.DCaseClass (Just pos) (P.UIdent name) args) = DataConstructor {constructorName = name, constructorParameters = map transformTypedParam args, constructorPos = pos}
transformConstructor (P.DCaseObject (Just pos) (P.UIdent name)) = DataConstructor {constructorName = name, constructorParameters = [], constructorPos = pos}

transformTypedParam :: P.TypedArg ParsedPos -> TypedParameter
transformTypedParam (P.TypedArgument (Just pos) (P.LIdent name) tpe) = TypedParameter {typedParameterType = transformType Star tpe, typedParameterName = name, typedParameterPos = pos} 

transformParam :: P.Arg ParsedPos -> Parameter
transformParam (P.Argument (Just pos) (P.LIdent name) tpe) = Parameter {parameterType = transformMaybeTyped tpe, parameterName = name, parameterPos = pos}

transformExpr :: P.Expr ParsedPos -> Expr
transformExpr (P.EIntLit (Just pos) val) = EIntLit {ival = val,  exprPos=pos}
transformExpr (P.ENegativeIntLit (Just pos) val) = EIntLit {ival = -val,  exprPos=pos}
transformExpr (P.EStringLit (Just pos) val) = EStringLit {sval = val,  exprPos=pos}
transformExpr (P.EVal (Just pos) (P.LIdent name)) = EVal {targetName= name,  exprPos=pos}
transformExpr (P.EApp (Just pos) (P.LIdent name) args) = transformApp pos (EVal {targetName=name,  exprPos=pos}) args
transformExpr (P.ECaseObject (Just pos) (P.UIdent name)) = EVal {targetName=name,  exprPos=pos}
transformExpr (P.ECaseClass (Just pos) (P.UIdent name) args) = transformApp pos (EVal {targetName=name,  exprPos=pos}) args
transformExpr (P.EIf (Just pos) predicate ifTrue ifFalse) = EIf { predicate = transformExpr predicate, ifTrue= transformExpr ifTrue, ifFalse = transformExpr ifFalse, exprPos=pos}
transformExpr (P.ELambda (Just pos) args expr) = transformLambda pos args expr
transformExpr (P.EMatch (Just pos) expr cases) = EMatch { target= transformExpr expr, cases = map transformCase cases, exprPos=pos}
transformExpr (P.EBlock (Just pos) defs expr) = transformDefs defs expr

transformApp :: Pos -> Expr -> [P.Expr ParsedPos] -> Expr
transformApp pos target' [h] = EApp { target=target', argument=transformExpr h, exprPos=pos}
transformApp pos target' (h:t) = transformApp pos (EApp { target=target' , argument= transformExpr h, exprPos=pos}) t


transformLambda :: Pos -> [P.Arg ParsedPos] -> P.Expr ParsedPos  -> Expr
transformLambda pos [h] body' = ELambda {parameter = transformParam h, body = transformExpr body' , exprPos=pos}
transformLambda pos (h:t) body' = ELambda { parameter = transformParam h, body = transformLambda pos t body', exprPos = pos}

transformCase :: P.Case ParsedPos -> Case
transformCase (P.CCase (Just pos) pat expr) = Case (transformPattern pat) (transformExpr expr) pos

transformPattern :: P.Pattern ParsedPos -> Pattern
transformPattern (P.PCaseObject (Just pos) (P.UIdent name)) = PData {patName = name, subpatterns = [], patPos = pos}
transformPattern (P.PCaseClass (Just pos) (P.UIdent name) subpatterns) = PData {patName = name, subpatterns = map transformPattern subpatterns, patPos = pos}
transformPattern (P.PCapture (Just pos) (P.LIdent name)) = PCapture {patName = name, patPos = pos}
transformPattern (P.PIntLit (Just pos) val) = PIntLit {pival = val, patPos = pos}
transformPattern (P.PNegativeIntLit (Just pos) val) = PIntLit {pival = -val, patPos = pos}
transformPattern (P.PStringLit (Just pos) val) = PStringLit {psval = val, patPos = pos}

transformDefs :: [P.Def ParsedPos] -> P.Expr ParsedPos -> Expr
transformDefs [] expr = transformExpr expr
transformDefs (h:t) expr = let transformedDef = transformDef h in EDef {
  def = transformedDef,
  body = transformDefs t expr,
  exprPos = defPos transformedDef
}

transformProgram :: P.Program ParsedPos -> Expr
transformProgram (P.ProgramDef _ defs expr) = transformDefs defs expr