all:
	cabal build -j1
	mv dist/build/gluszec-impl-exe/gluszec-impl-exe interpreter

clean:
	cabal clean
	rm interpreter